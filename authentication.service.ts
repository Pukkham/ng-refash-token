import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { RootScopeService } from './root-scope.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private url = '';
  constructor(private http: HttpClient, private router: Router, private root: RootScopeService) {
    // this.url = environment.api + environment.path;
    this.url = environment.api + '/api';
  }

  /**
   * get header seted
   * @access private
   * @return HttpHeaders
   */
  private getHeader(): any {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + localStorage.getItem('token') || '',
        'Content-Type': 'application/json'
      })
    };

    return httpOptions;
  }

  /**
   * Get toker string
   * @access public
   * @return string
   */
  public getToken(): string {
    let token = '';
    if (localStorage.getItem('token')) {
      token = localStorage.getItem('token');
    }
    return token;
  }

  /**
   * Get toker string
   * @access public
   * @return string
   */
  public getRefreshToken(): string {
    let token = '';
    if (localStorage.getItem('refreshToken')) {
      token = localStorage.getItem('refreshToken');
    }
    return token;
  }

  /**
   * Check logined
   * @accessed public
   * @returns Observable any
   */
  public checkLogin(): Observable<any> {
    const url = this.url + '/login';
    return this.http.get<any>(url, this.getHeader());
  }

  /**
   * Get role name
   * @access public
   * @return Observable
   */
  public role(): Observable<any> {
    const url = this.url + '/role';
    return this.http.get<any>(url, this.getHeader());
  }

  /**
   * Post to login
   * @params param
   * @accessed public
   * @returns Observable any
   */
  public login(param: any): Observable<any> {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    const url = this.url + '/auth/token';
    if (param) {
      param.grant_type = 'password';
      param.login_type = 'admin';
    }

    return this.http.post<any>(url, param, headers).pipe(
      tap((_: any) => {
        localStorage.setItem('token', _.token.access_token);
        localStorage.setItem('refreshToken', _.token.refresh_token);
      }), catchError(this.handleError<any>('Login'))
    );
  }

  /**
   * Post to login startbiz
   * @params param
   * @access public
   * @return any
   */
  public loginStartbiz(param: any): Observable<any> {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json',
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: '0'
      })
    };

    const url = 'http://localhost:7777/portalserver/j_spring_security_check';
    const login = {
      potal_name: 'scb-casa',
      page_name: 'page_1490514300183',
      j_username: '',
      j_password: ''
    };
    if (param) {
      login.j_username = param.username;
      login.j_password = param.password;
    }

    return this.http.post<any>(url, login, headers).pipe(
      tap((_: any) => {
        console.log(_);
        // localStorage.setItem('token', _.access_token);
        // localStorage.setItem('refreshToken', _.refresh_token);
      }), catchError(this.handleError<any>('Login'))
    );
  }

  /**
   * Refresh Token
   * @access public
   * @return Observable
   */
  public refreshToken(): Observable<any> {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    const url = this.url + '/auth/token';
    const param = {
      login_type: 'admin',
      grant_type: 'refresh_token',
      refresh_token: localStorage.getItem('refreshToken')
    };
    return this.http.post<any>(url, param, headers).pipe(
      tap((_: any) => {
        localStorage.setItem('token', _.token.access_token);
        localStorage.setItem('refreshToken', _.token.refresh_token);
      }), catchError(this.handleError<any>('Login'))
    );
  }

  /**
   * Get role of user login
   * @access public
   * @return any
   */
  public async getRole(): Promise<any> {
    const user = await this.decode();
    // TODO: Return role from token when token has a role in db.
    for (const role in user.realm_access.roles) {
      if (user.realm_access.roles[role].indexOf('ADMIN') >= 0) {
        return 'admin';
      }
    }
    return null;
  }

  /**
   * Check is have user loging in
   * @access public
   * @return boolean
   */
  public async isLogin(): Promise<boolean> {
    const user = await this.decode();

    if (user) {
      await this.root.setIsLogin(true);
      return true;
    }

    await this.root.setIsLogin(false);
    return false;
  }

  /**
   * Decode jwt
   * @access public
   * @return object user
   */
  public async decode(): Promise<any> {
    const token = localStorage.getItem('token');
    if (!token) {
      return false;
    }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map((c) => {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  /**
   * Logout
   * @access public
   * @return void
   */
  public async logout(): Promise<any> {
    const url = this.url + '/logout';
    // await this.http.get<any>(url).toPromise();
    await this.root.setIsLogin(false);
    localStorage.clear();
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: Look error status
      console.log(error.status);

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a LoginService message with the MessageService */
  private log(message: string) {
    console.log(`LoginService: ${message}`);
  }

  public async getUserInfo() {
    const user = await this.decode();
    return user;
  }

}
